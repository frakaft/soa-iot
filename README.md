# soa-iot
Sistema distribuido para la catedra de Arquitectura Orientada a Servicios de la carrera de Ingenieria Informatica.

## Consigna
Implementar un sistema distribuido con las siguientes características y componentes mínimos:
1. Componente de autenticación
    * Sistema de autenticación por token.
2. Almacenamiento histórico
3. Broker MQTT
4. Componente de sistema embebido:
    * Deberá leer valores de sensores y poder trabajar con actuadores (leds, motores, etc).
    * El componente deberá pedir un token al SAT para poder almacenar los valores obtenidos de los sensores.
    * Los valores leídos serán publicados en BM, el o los tópicos quedan a criterio del grupo. 
    * Si al componente se le requiere interactuar de alguna manera con un actuador, deberá recibir el token del componente cliente y validarlo contra SAT (bridge), solo si el token es válido se ejecutará la acción. Se debe definir una API REST (síncrona) y comandos via MQTT (asíncrono) para interactuar con actuadores. En ambos casos es necesario definir como recibir el token, en el caso particular de MQTT se informará de alguna manera fiable, utilizando el mismo protocolo (MQTT), el resultado de la operación. Atención: se deberá diseñar una combinación de tópico y mensaje tal que el cliente que consumió el servicio, pueda conocer el resultado y que no se confunda con otros mensajes y clientes. En el caso de REST, solo debe seguirse la recomendación estándar.
5. Cliente
    * Desarrollar un cliente que permita obtener el último valor leído por CSE de forma síncrona y de forma asíncrona, también un historial de valores e interactuar con actuadores del CSE.
    * El cliente debera pedir credenciales (login) a partir de las cuales obtendrán un token del sistema de autenticación a fin de poder consumir servicios los diferentes servicios.



### Arquitectura
![](images/arch.jpg)

### Componentes y funcionamiento
| Componente | Funcionamiento |
| ----------- | ----------- |
| Broker | Broker MQTT implementado mediante Node Red. Disponibiliza dos colas de mensajes, en una de ellas el sistema embebido publica los datos capturados por sus sensores, y en otra de ellas el sistema embebido está suscripto escuchado que colores mostrar. <ul><li>En el archivo [node-red/broker.json](node-red/rpi.json) encontraran el flujo de node red para poder implementar un broker.</li><li>[ Ver configuracion broker en Node Red.](https://github.com/martin-doyle/node-red-contrib-aedes/)</li><li>[Ver configuracion certificados SSL y securizacion en Node Red.](https://nodered.org/docs/user-guide/runtime/securing-node-red/)</li></ul> |
| Sistema embebido | Sistema embebido con dos elementos, un sensor de temperatura y humedad AM2302(o DHT22) y un LED RGB. El sistema realiza dos principales funciones, por un lado, lee los datos del sensor y publica los datos en una cola de mensajes, y por el otro está suscripto a otra cola de mensajes donde espera el código hexadecimal del color a mostrar en el LED RGB. <ul><li>En el archivo [node-red/rpi.json](node-red/rpi.json) encontraran el flujo de node red ejecutado por el componente.</li><li> [Ver diagrama de conexion.](#diagrama-conexion-raspberry-pi)</li></ul> |
| API de autenticacion | Instancia de Fusion Auth (on prem) que brinda una API de autenticación por tokens JWT. <ul><li>[Ver API Fusion Auth.](https://fusionauth.io/docs/v1/tech/apis/)</li><li>En el directorio [api-fusion-auth](api-fusion-auth) encontraran los archivos de despligue de la API.</li><li>Para lanzar la API correr en una terminal: ```docker-compose up ```</li></ul> |
| API general | Se cuenta con una api de general que tiene distintas responsabilidades. <ul><li>Embasador API autenticación: Brinda endpoints de autenticación y validación de tokens que se comunican con la API de autenticación.</li><li>Persistencia lectura de sensores: Está suscripto al tópico donde el sistema embebido publica los valores de temperatura y humedad, los interpreta y persiste en una base de datos relacional.</li><li>Consulta de datos: Brinda un endpoint para poder consultar los valores persistidos de los sensores.</li><li>Configuración de color: Brinda un endpoint que recibe el color a mostrar por el LED RGB, y lo publica en el tópico al que el sistema embebido está suscripto a la espera del código del color hexadecimal a mostrar.</li></ul>La API se encuentra segurizada con certificados generados por Let' s Encrypt generados por Certbot e implementados mediante un proxy reverso Nginx.<ul><li>[Ver como generar certificados SSL con Certbot.](https://certbot.eff.org/instructions?ws=other&os=debianbuster/)</li><li>[Ver configuracion de certificados SSL en Nginx.](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/)</li><li>[Ver configuracion proxy reverso en Nginx.](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)</li></ul> Configuracion y ejecucion: <ul><li>En el directorio [nginx.conf](nginx.conf) encontraran los archivos de configuracion de nginx correspondientes.</li><li>En el directorio [api-general](api-general) encontraran los archivos fuentes de la API.</li><li>Para lanzar la API correr en una terminal: ```docker-compose up ```</li></ul> |
| Cliente | Cliente desarrollado en Angular. Se auntentica con el sistema de autenticacion, y permite consultar los datos almacenados por los sensores y configurar el color a mostrar por el componente de sistema embebido. |

### Diagrama conexion Raspberry Pi
![](images/rpi.jpg)