CREATE TABLE timestamp
(
    id    BIGSERIAL      NOT NULL,
    value TIMESTAMPTZ(0) NOT NULL,

    CONSTRAINT timestamp_pk PRIMARY KEY (id)
);

CREATE TABLE temperature
(
    id           BIGSERIAL NOT NULL,
    value        FLOAT     NOT NULL,
    timestamp_id BIGINT    NOT NULL,

    CONSTRAINT temperature_pk PRIMARY KEY (id),
    CONSTRAINT temperature_fk_timestamp FOREIGN KEY (timestamp_id) REFERENCES timestamp (id)
);

CREATE TABLE humidity
(
    id           BIGSERIAL NOT NULL,
    value        FLOAT     NOT NULL,
    timestamp_id BIGINT    NOT NULL,

    CONSTRAINT humidity_pk PRIMARY KEY (id),
    CONSTRAINT humidity_fk_timestamp FOREIGN KEY (timestamp_id) REFERENCES timestamp (id)
);