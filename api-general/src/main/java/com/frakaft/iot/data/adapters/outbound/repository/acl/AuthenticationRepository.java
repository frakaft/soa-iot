package com.frakaft.iot.data.adapters.outbound.repository.acl;

import com.frakaft.iot.data.adapters.outbound.repository.acl.dto.FusionAuthLoginRequest;
import com.frakaft.iot.data.adapters.outbound.repository.acl.dto.FusionAuthLoginResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Repository
@RequiredArgsConstructor
public class AuthenticationRepository {

    private final RestTemplate restTemplate;

    @Value("${fusionauth.url}")
    private String fusionauthUrl;

    public FusionAuthLoginResponse login(FusionAuthLoginRequest fusionAuthLoginRequest) {
        try {
            return restTemplate.postForObject(
                    fusionauthUrl + "/api/login",
                    fusionAuthLoginRequest,
                    FusionAuthLoginResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void validateToken(String token) throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        restTemplate.exchange(
                RequestEntity.get(new URI(fusionauthUrl + "/api/jwt/validate")).headers(headers).build(),
                String.class);
    }
}
