package com.frakaft.iot.data.adapters.outbound.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.frakaft.iot.data.adapters.outbound.producer.message.ColorMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Slf4j
@Component
@RequiredArgsConstructor
public class ColorProducer {

    private final IMqttClient iMqttClient;

    @Value("${mqtt.topic.color}")
    private String mqttTopicColor;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new JavaTimeModule());

    @SneakyThrows
    public void sendMessage(ColorMessage colorMessage) {
        if (!iMqttClient.isConnected()) {
            throw new RuntimeException("NOT CONNECTED");
        }
        byte[] payload = OBJECT_MAPPER.writeValueAsString(colorMessage).getBytes(StandardCharsets.UTF_8);
        MqttMessage msg = new MqttMessage(payload);
        msg.setQos(0);
        msg.setRetained(true);
        iMqttClient.publish(mqttTopicColor, msg);
        log.info(colorMessage.toString());
    }
}
