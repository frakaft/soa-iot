package com.frakaft.iot.data.domain.model;

import lombok.*;

import java.time.OffsetDateTime;

@Getter
@Setter
@Builder
public class Data {
    private Float temperature;
    private Float humidity;
    private OffsetDateTime timestamp;
}
