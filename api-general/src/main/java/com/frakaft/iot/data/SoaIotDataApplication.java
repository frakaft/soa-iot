package com.frakaft.iot.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoaIotDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoaIotDataApplication.class, args);
    }
}
