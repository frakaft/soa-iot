package com.frakaft.iot.data.adapters.inbound.request.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.regex.Pattern;

public class HexColorValidator implements ConstraintValidator<HexColor, String> {

    private Pattern colorPattern;

    @Override
    public void initialize(HexColor constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        colorPattern = Pattern.compile("#([0-9a-f]{3}|[0-9a-f]{6}|[0-9a-f]{8})");
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        try {
            return colorPattern.matcher(value).matches();
        } catch (Exception e) {
            return false;
        }
    }
}
