package com.frakaft.iot.data.adapters.inbound.exceptions;

import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;

@Getter
@Setter
@Builder
public class ApiError {
    private final OffsetDateTime timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;

    public static ResponseEntity<ApiError> build(HttpStatus httpStatus, Exception e, WebRequest request) {
        return ResponseEntity.status(httpStatus).body(ApiError.builder()
                .timestamp(OffsetDateTime.now())
                .status(httpStatus.value())
                .error(httpStatus.getReasonPhrase())
                .message(e.getMessage())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .build());
    }
}

