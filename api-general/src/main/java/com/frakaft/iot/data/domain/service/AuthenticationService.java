package com.frakaft.iot.data.domain.service;

import com.frakaft.iot.data.adapters.inbound.request.LoginRequest;
import com.frakaft.iot.data.adapters.outbound.repository.acl.AuthenticationRepository;
import com.frakaft.iot.data.adapters.outbound.repository.acl.dto.FusionAuthLoginRequest;
import com.frakaft.iot.data.domain.exceptions.GeneralException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static com.frakaft.iot.data.utils.TokenUtils.sanitize;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final AuthenticationRepository authenticationRepository;

    @Value("${fusionauth.application.id}")
    private String fusionauthApplicationId;

    public String login(LoginRequest loginRequest) {
        try {
            return authenticationRepository.login(FusionAuthLoginRequest.builder()
                    .loginId(loginRequest.getUsername())
                    .password(loginRequest.getPassword())
                    .applicationId(fusionauthApplicationId)
                    .build()).getToken();
        } catch (Exception e) {
            throw GeneralException.builder(HttpStatus.UNAUTHORIZED, "Invalid username or password");
        }
    }

    public void validateToken(String token) {
        try {
            authenticationRepository.validateToken(sanitize(token));
        } catch (Exception e) {
            throw GeneralException.builder(HttpStatus.UNAUTHORIZED, "Invalid token");
        }
    }
}
