package com.frakaft.iot.data.domain.service;

import com.frakaft.iot.data.adapters.inbound.request.ColorRequest;
import com.frakaft.iot.data.adapters.inbound.suscriber.message.DataRequest;
import com.frakaft.iot.data.adapters.outbound.producer.ColorProducer;
import com.frakaft.iot.data.adapters.outbound.producer.message.ColorMessage;
import com.frakaft.iot.data.adapters.outbound.repository.data.DataRepository;
import com.frakaft.iot.data.domain.exceptions.GeneralException;
import com.frakaft.iot.data.domain.model.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DataService {

    private final DataRepository dataRepository;
    private final ColorProducer colorProducer;

    public List<Data> getData(Integer size, Long offset) {
        try {
            return dataRepository.getData(size, offset);
        } catch (Exception e) {
            throw GeneralException.builder(HttpStatus.CONFLICT, "Data could not be retrieved");
        }
    }

    @Transactional
    public void saveData(DataRequest dataRequest) {
        try {
            Long timestampId = dataRepository.saveTimestamp(dataRequest.getTimestamp());
            dataRepository.saveTemperature(dataRequest.getTemperature(), timestampId);
            dataRepository.saveHumidity(dataRequest.getHumidity(), timestampId);
        } catch (Exception e) {
            throw GeneralException.builder(HttpStatus.CONFLICT, "Data could not be saved");
        }
    }

    public void setColor(ColorRequest colorRequest, String token) {
        try {
            colorProducer.sendMessage(ColorMessage.builder()
                    .color(colorRequest.getColor())
                    .token(token).build());
        } catch (Exception e) {
            throw GeneralException.builder(HttpStatus.CONFLICT, "Color could not be set");
        }
    }
}
