package com.frakaft.iot.data.domain.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

@Getter
public class GeneralException extends HttpStatusCodeException {

    public GeneralException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public static GeneralException builder(HttpStatus httpStatus, String message) {
        return new GeneralException(httpStatus, message);
    }
}
