package com.frakaft.iot.data.adapters.inbound.api;

import com.frakaft.iot.data.adapters.inbound.request.ColorRequest;
import com.frakaft.iot.data.adapters.inbound.response.DataResponse;
import com.frakaft.iot.data.domain.model.Data;
import com.frakaft.iot.data.domain.service.AuthenticationService;
import com.frakaft.iot.data.domain.service.DataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
public class IOTController {

    private final DataService dataService;
    private final AuthenticationService authenticationService;

    @GetMapping("/iot")
    public DataResponse getData(
            @RequestParam(value = "size", defaultValue = "1") @Min(1) Integer size,
            @RequestParam(value = "offset", defaultValue = "0") @Min(0) Long offset,
            @RequestHeader(value = HttpHeaders.AUTHORIZATION) @NotBlank String token) {

        authenticationService.validateToken(token);
        List<Data> data = dataService.getData(size, offset);
        return DataResponse.builder()
                .data(data)
                .size(data.size())
                .build();
    }

    @PostMapping("/color")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setColor(
            @RequestBody @Valid ColorRequest colorRequest,
            @RequestHeader(value = HttpHeaders.AUTHORIZATION) @NotBlank String token) {

        authenticationService.validateToken(token);
        dataService.setColor(colorRequest, token);
    }
}
