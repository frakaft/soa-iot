package com.frakaft.iot.data.adapters.outbound.repository.acl.dto;

import lombok.*;

@Getter
@Setter
@Builder
public class FusionAuthLoginRequest {
    private String loginId;
    private String password;
    private String applicationId;
}
