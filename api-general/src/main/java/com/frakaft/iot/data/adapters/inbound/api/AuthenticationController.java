package com.frakaft.iot.data.adapters.inbound.api;

import com.frakaft.iot.data.adapters.inbound.request.LoginRequest;
import com.frakaft.iot.data.adapters.inbound.response.LoginResponse;
import com.frakaft.iot.data.domain.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@Validated
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public LoginResponse login(@RequestBody @Valid LoginRequest loginRequest) {
        return LoginResponse.builder().token(authenticationService.login(loginRequest)).build();
    }

    @GetMapping("/token-validation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void validateToken(@RequestParam(value = "token") @NotNull @NotBlank String token) {
        authenticationService.validateToken(token);
    }
}
