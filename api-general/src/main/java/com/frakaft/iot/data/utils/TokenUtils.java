package com.frakaft.iot.data.utils;

public class TokenUtils {

    public static String sanitize(String token) {
        return token.replaceFirst("(?i)bearer\\s?", "");
    }
}
