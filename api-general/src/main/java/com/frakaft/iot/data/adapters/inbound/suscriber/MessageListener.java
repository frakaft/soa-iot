package com.frakaft.iot.data.adapters.inbound.suscriber;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.frakaft.iot.data.adapters.inbound.suscriber.message.DataRequest;
import com.frakaft.iot.data.domain.service.AuthenticationService;
import com.frakaft.iot.data.domain.service.DataService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageListener {

    private final DataService dataService;
    private final AuthenticationService authenticationService;
    private final IMqttClient iMqttClient;

    @Value("${mqtt.topic.sensors}")
    private String mqttTopicSensors;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new JavaTimeModule());

    @PostConstruct
    @SneakyThrows
    public void listen() {
        iMqttClient.subscribe(mqttTopicSensors, (topic, message) -> {
            try {
                DataRequest dataRequest = OBJECT_MAPPER.readValue(new String(message.getPayload(), StandardCharsets.UTF_8), DataRequest.class);
                authenticationService.validateToken(dataRequest.getToken());
                dataService.saveData(dataRequest);
                log.info(dataRequest.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
