package com.frakaft.iot.data.adapters.inbound.request.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(
        validatedBy = {HexColorValidator.class}
)
@Documented
public @interface HexColor {

    String message() default "Not a valid hex color";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
