package com.frakaft.iot.data.adapters.outbound.repository.acl.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FusionAuthLoginResponse {
    private String token;
}
