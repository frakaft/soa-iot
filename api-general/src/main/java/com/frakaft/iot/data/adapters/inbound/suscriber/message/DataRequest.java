package com.frakaft.iot.data.adapters.inbound.suscriber.message;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Getter
@Setter
public class DataRequest {
    @NotNull
    private Float temperature;
    @NotNull
    private Float humidity;
    @NotNull
    private OffsetDateTime timestamp;
    @NotNull
    private String token;
}
