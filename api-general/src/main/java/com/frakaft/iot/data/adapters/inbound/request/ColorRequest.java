package com.frakaft.iot.data.adapters.inbound.request;

import com.frakaft.iot.data.adapters.inbound.request.validator.HexColor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ColorRequest {
    @NotNull
    @HexColor
    private String color;
}
