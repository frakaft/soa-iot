package com.frakaft.iot.data.adapters.outbound.repository.data;

import com.frakaft.iot.data.domain.model.Data;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Mapper
@Repository
public interface DataRepositoryMyBatys {

    @Select("SELECT\n" +
            "\ttemperature.value as temperature,\n" +
            "\thumidity.value as humidity,\n" +
            "\ttimestamp.value as timestamp\n" +
            "FROM timestamp\n" +
            "\tINNER JOIN temperature ON temperature.timestamp_id = timestamp.id\n" +
            "\tINNER JOIN humidity ON humidity.timestamp_id = timestamp.id\n" +
            "ORDER BY timestamp.value DESC\n" +
            "LIMIT ${size}\n" +
            "OFFSET ${offset}")
    List<Data> getData(@Param("size") Integer size, @Param("offset") Long offset);

    @Select("INSERT INTO timestamp(id, value)\n" +
            "VALUES(DEFAULT, '${timestamp}')\n" +
            "RETURNING id")
    Long saveTimestamp(@Param("timestamp") OffsetDateTime timestamp);

    @Select("INSERT INTO temperature(id, value, timestamp_id)\n" +
            "VALUES(DEFAULT, #{temperature}, #{timestampId})\n" +
            "RETURNING id")
    Long saveTemperature(@Param("temperature") Float temperature, @Param("timestampId") Long timestampId);

    @Select("INSERT INTO humidity(id, value, timestamp_id)\n" +
            "VALUES(DEFAULT, #{humidity}, #{timestampId})" +
            "\nRETURNING id")
    Long saveHumidity(@Param("humidity") Float humidity, @Param("timestampId") Long timestampId);
}
