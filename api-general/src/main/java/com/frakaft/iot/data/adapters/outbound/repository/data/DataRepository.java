package com.frakaft.iot.data.adapters.outbound.repository.data;

import com.frakaft.iot.data.domain.model.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class DataRepository {

    private final DataRepositoryMyBatys dataRepositoryMyBatys;

    public List<Data> getData(@NonNull Integer size, @NonNull Long offset) {
        return dataRepositoryMyBatys.getData(size, offset);
    }

    public Long saveTimestamp(@NonNull OffsetDateTime timestamp) {
        return dataRepositoryMyBatys.saveTimestamp(timestamp);
    }

    public Long saveTemperature(@NonNull Float temperature, @NonNull Long timestampId) {
        return dataRepositoryMyBatys.saveTemperature(temperature, timestampId);
    }

    public Long saveHumidity(@NonNull Float humidity, @NonNull Long timestampId) {
        return dataRepositoryMyBatys.saveHumidity(humidity, timestampId);
    }
}
