package com.frakaft.iot.data.adapters.inbound.response;

import com.frakaft.iot.data.domain.model.Data;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class DataResponse {
    private List<Data> data;
    private Integer size;
}
