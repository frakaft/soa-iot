package com.frakaft.iot.data.adapters.outbound.producer.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ColorMessage {
    private String color;
    private String token;
}
