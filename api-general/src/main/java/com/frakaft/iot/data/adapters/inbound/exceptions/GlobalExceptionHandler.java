package com.frakaft.iot.data.adapters.inbound.exceptions;

import com.frakaft.iot.data.domain.exceptions.GeneralException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({NoSuchElementException.class})
    public ResponseEntity<ApiError> noSuchElementException(NoSuchElementException e, WebRequest request) {
        return ApiError.build(HttpStatus.NOT_FOUND, e, request);
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<ApiError> nullPointerException(NullPointerException e, WebRequest request) {
        return ApiError.build(HttpStatus.NOT_FOUND, e, request);
    }

    @ExceptionHandler({GeneralException.class})
    public ResponseEntity<ApiError> generalException(GeneralException e, WebRequest request) {
        return ApiError.build(e.getStatusCode(), e, request);
    }
}
