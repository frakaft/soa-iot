﻿export class Sensors {
    temperature: number;
    humidity: number;
    timestamp: Date;
}