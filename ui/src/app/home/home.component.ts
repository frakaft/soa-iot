﻿import { Component } from '@angular/core';

import { User, Color, Page } from '@app/_models';
import { AccountService } from '@app/_services';
import { AlertService } from '@app/_services';
import { interval, Subscription } from 'rxjs';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    user: User;
    page: Page;
    color: Color;
    subscription: Subscription;

    constructor(
        private accountService: AccountService,
        private alertService: AlertService) {
        this.user = this.accountService.userValue;
    }

    async ngOnInit() {
        this.page = await this.accountService.sensors(10, 10);
        console.log(this.page);
        const source = interval(10000);
        this.subscription = source.subscribe(val => console.log(val));
    }

    async colorChange(event) {
        console.log(event);
        await this.accountService.color(event.hexString);
    }
}